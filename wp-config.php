<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'tincman_test');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

define('WP_HOME','http://tincman.test/');
define('WP_SITEURL','http://tincman.test/');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '>W+J+{#rP+MZ3Etm#QNHQ~~V$xSFe{t1hAFGfi;Tm&`L*qY6DK78IjZ-wKJ?/{r3');
define('SECURE_AUTH_KEY',  ' O^K0Apg1pME5zyKITP6mttRmtYC/UGfJ@OqARJ>qZN ?bG0od$[P4q UXhXynY9');
define('LOGGED_IN_KEY',    ' D5TJ?Ki/hHcucT2v.3d2HbhI-pbTnIbk)xT#- cl^,BGC3sNx!mgKdae^TaT|v!');
define('NONCE_KEY',        'Mo3@c<ZLW)xqK8a]+Lfe1H>]B%s.(|%^.bvUk}#{^P`6RJ|S5{1MAj)SSd%#Pyr/');
define('AUTH_SALT',        '[Qlp nm-bXzp6v+M*l+N$.+NgXvTLgiay=-hhIEeBxfD$^zFA3t|9^PvIs<gY&FQ');
define('SECURE_AUTH_SALT', '$fk#Qzntd~WYcEqzWm5$(H{v%w>1O=pN#c_^~DkoLd]gr$pac3*:j+Dh,jq*uIQh');
define('LOGGED_IN_SALT',   '53z$5NBCK90@hvOO{ 8;&n?r[_+`fO=$4_G06Si [hLl([WJ1b)i>y1o<Kh+(CXF');
define('NONCE_SALT',       'T!XB4R-)NMk1@$*])<eKrg/O4c#TC16VQUL|e hyl7/jMnATs0C[_N2O2+@,m%Ko');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');