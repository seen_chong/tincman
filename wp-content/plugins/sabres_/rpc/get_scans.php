<?php

require_once SABRES_PLUGIN_DIR . '/library/fail.php';
require_once SABRES_PLUGIN_DIR . '/_inc/modules/class.malware.scanner.engine.php';

class Get_Scans {
    public function execute($rpc_data) {
      $scanner = SBS_Malware_Scanner::getInstance();
      $scans = $scanner->get_scans_by_status( 'running' );

      if ( !empty( $scans ) ) {
          $res = json_encode( array_values( $scans ) );
          echo $res;
      }
    }

}
