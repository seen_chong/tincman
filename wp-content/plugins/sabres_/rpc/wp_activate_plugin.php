<?php

require_once SABRES_PLUGIN_DIR . '/library/fail.php';
require_once ABSPATH.'/wp-load.php';
require_once ( ABSPATH . '/wp-admin/includes/admin.php' );

class WP_Activate_Plugin {

  public function execute($rpc_data) {
    $activate=true;
    $arg=@$rpc_data['status'];
    if (!empty($arg)) {
      if (strcasecmp($arg,'off')==0)
        $activate=false;
      else if (strcasecmp($arg,'on')==0)
        $activate=true;
      else
        SBS_Fail::byeArr(array( 'message'=>"RPC: Invalid status parameter: $arg",
                              'code'=>400
                             ));
    }
    else
      SBS_Fail::byeArr(array( 'message'=>"RPC: Missing status parameter",
                          'code'=>400
                         ));
    if ($activate) {
      @file_put_contents(SABRES_PLUGIN_DIR.'/activation.log.txt',date("Y-m-d H:i:s").' Plugin activated by RPC '.PHP_EOL,FILE_APPEND | LOCK_EX);
      $result=activate_plugin(SABRES_PLUGIN_MAIN_FILE);
      if (is_wp_error($result))
          SBS_Fail::bye('WP_Activate_Plugin: '.$result->get_error_message(),$result->get_error_data());
      else
        echo "Plugin activated";
    }
    else {
          @file_put_contents(SABRES_PLUGIN_DIR.'/activation.log.txt',date("Y-m-d H:i:s").' Plugin deactivated by RPC '.PHP_EOL,FILE_APPEND | LOCK_EX);
          deactivate_plugins(SABRES_PLUGIN_MAIN_FILE);
          if (!is_plugin_active(SABRES_PLUGIN_BASE_NAME))
            echo "Plugin deactivated";
          else
            echo "Plugin deactivation failed";
       }



  }
}
