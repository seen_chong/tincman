<?php

require_once SABRES_PLUGIN_DIR . '/library/fail.php';
require_once SABRES_PLUGIN_DIR . '/_inc/sbr_utils.php';
require_once ABSPATH.'/wp-load.php';

class Plugins_Inventory {

    public function execute($rpc_data) {
        SbrUtils::validate_inventory_can_execute();
        $files = null;
//        $exts = array( '.php' );
        $exts = null;

        if ( !empty( $rpc_data['files'] ) ) {
            $files = $rpc_data['files'];
        }

        if ( !empty ($rpc_data['exts'] ) ) {
            $raw_exts = array();
            foreach( explode( ';', $rpc_data['exts'] ) as $ext ) {
                  array_push( $raw_exts, $ext );
            }

            $exts = count($raw_exts) ? $raw_exts : $exts;
        }

        $res = json_encode( $this->get_plugins( $files, $exts ) );
        echo $res;
    }



    public function get_plugins( $files = null, $exts )
    {
        if ( !function_exists( 'get_plugins' ) ) {
            require_once ABSPATH . 'wp-admin/includes/plugin.php';
        }

        $get_files = ( isset( $files ) && strcasecmp( trim( $files ), 'true' ) == 0 );

        $plugins = get_plugins();
        foreach ( $plugins as $plugin_file => &$plugin_attr ) {
            $plugin_attr['Active'] = is_plugin_active( $plugin_file );
            $plugin_attr['path']=dirname(SbrUtils::find_relative_path(ABSPATH,WP_PLUGIN_DIR.'/'.$plugin_file));

            if ( $get_files ) {
                $files = array();
                $result_files=null;
                if (stripos($plugin_file,'/')!==false)
                    $result_files = SbrUtils::get_files( WP_PLUGIN_DIR.'/'.dirname( $plugin_file ), $exts, true );
                else
                    $result_files=array(WP_PLUGIN_DIR.'/'.$plugin_file); //plugin contains just one file in the root plugins folder (hello dolly)

                list( $result_files, $failed_files) = SbrUtils::exclude_no_readable( $result_files );

                if ( count( $failed_files ) ) {
                    $logger = SBS_Logger::getInstance();
                    $logger->log( 'warning', "RPC", "Plugins inventory", implode( ", ", $failed_files ) );
                }

                foreach ( $result_files as $result_file ) {
                    if ( !is_dir( $result_file ) ) {
                        $file_data=array(
                            'Name' => str_replace(WP_PLUGIN_DIR.'/', '', $result_file ));
                        SbrUtils::calc_md5_file( $result_file ,$file_data);
                        $files[]=$file_data;
                    }
                }
                $plugin_attr['Files'] = $files;
            }
        }

        return $plugins;
    }

}
