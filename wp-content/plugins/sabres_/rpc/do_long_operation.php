<?php

require_once SABRES_PLUGIN_DIR . '/library/fail.php';
require_once SABRES_PLUGIN_DIR . '/modules/long_op.php';

class Sabres_Do_Long_Operation {
  public function execute($rpc_data) {
    if ( empty( $rpc_data['op_val'] ) ) {
      SBS_Fail::byeArr(array( 'message'=>"RPC: Missing op_val parameter",
                              'code'=>400
                             ));
    }
    if ( empty( $rpc_data['secs'] ) ) {
      SBS_Fail::byeArr(array( 'message'=>"RPC: Missing secs parameter",
                              'code'=>400
                             ));
    }
    Sabres_Long_Operation::do_long_op($rpc_data['op_val'],intval($rpc_data['secs']));        

  }

}
