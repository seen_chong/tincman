<?php

require_once SABRES_PLUGIN_DIR . '/library/fail.php';
require_once SABRES_PLUGIN_DIR . '/_inc/sbr_utils.php';

class Core_Inventory {

    public function execute($rpc_data) {

      SbrUtils::validate_inventory_can_execute();

      $files = null;
//      $exts = array( '.php' );
      $exts = null;

      if ( !empty( $rpc_data['files'] ) ) {
          $files = $rpc_data['files'];
      }

      if ( !empty ($rpc_data['exts'] ) ) {
          $raw_exts = array();
          foreach( explode( ';', $rpc_data['exts'] ) as $ext ) {
              array_push( $raw_exts, $ext );
          }

          $exts = count($raw_exts) ? $raw_exts : $exts;
      }      

      $res = json_encode( $this->get_core( $files, $exts ) );

      echo $res;
    }


    public function get_core( $files = null, $exts )
    {
        require_once ABSPATH.'/wp-load.php';
        global $wp_version;

        $ret = array(
            'version' => $wp_version,
            'files' => array()
        );

        $get_files = ( isset( $files ) && strcasecmp( trim( $files ), 'true' ) == 0 );

        if ( $get_files ) {
            $files_inv = SbrUtils::get_files( rtrim( ABSPATH, '/\\' ), $exts );
            $files_inv = array_merge( $files_inv, SbrUtils::get_files( ABSPATH . 'wp-admin', $exts, true ) );
            $files_inv = array_merge( $files_inv, SbrUtils::get_files( ABSPATH . 'wp-includes', $exts, true ) );
            $files_inv = array_merge( $files_inv, SbrUtils::get_files( WP_CONTENT_DIR, $exts ) );

            $content_dirs = SbrUtils::get_dirs( WP_CONTENT_DIR  );
            $plugin_path = realpath( WP_PLUGIN_DIR );
            $theme_path = realpath( get_theme_root() );

            foreach ( $content_dirs as $dir ) {
                if ( is_dir( $dir ) ) {
                    if ( realpath( $dir ) !== $plugin_path && realpath( $dir ) != $theme_path ) {
                        $files_inv = array_merge( $files_inv, SbrUtils::get_files( $dir, $exts, true ) );
                    }
                }
            }

            list( $files_inv, $failed_files) = SbrUtils::exclude_no_readable( $files_inv );

            if ( count( $failed_files ) ) {
                $logger = SBS_Logger::getInstance();
                $logger->log( 'warning', "RPC", "Core inventory", implode( ", ", $failed_files ) );
            }

            foreach ( $files_inv as $file ) {
                if ( !is_dir( $file ) ) {
                    $file_name = ltrim( str_replace( rtrim( ABSPATH, '/\\' ), '', $file ), '/\\' );
                    $file_name = str_replace( '\\', '/', $file_name );

                    $file_data=array('fullPath' => $file_name);
                    SbrUtils::calc_md5_file($file,$file_data);
                    $ret['files'][] = $file_data;
                }
            }
        }

        return $ret;
    }



}
