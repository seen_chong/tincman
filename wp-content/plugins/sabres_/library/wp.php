<?php



class Sabres_WP {

  private static $instance;

  public static function instance() {
    if (self::$instance==null)
      self::$instance=new Sabres_WP();
    return self::$instance;
  }

  public function get_temp_dir() {
    $temp_dir = get_temp_dir();
    if (is_writable($temp_dir)) {
      return $temp_dir;
    }

    if (is_writable(SABRES_TMP_DIR)) {
      return SABRES_TMP_DIR;
    }

    return false;
  }

  public function file_put_contents_safe($output_filename,$lock_filename,$text_value) {
    require_once SABRES_PLUGIN_DIR.'/_inc/sbr_utils.php';
    $temp_dir = Sabres_WP::instance()->get_temp_dir();
    if ($temp_dir === false) {
      return new WP_Error('fpcs_failed_put_contents', "Can't find writable directory",debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS));
    }
    $temp_filename=tempnam($temp_dir ,'sbr_' );
    if ($temp_filename===FALSE || ! @file_put_contents($temp_filename, $text_value)) {
      $err = error_get_last();
      return new WP_Error('fpcs_failed_put_contents',$err['message'],debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS));
    }
    $lock_filename=SbrUtils::concatWithSingleSlash(array(
    Sabres_WP::instance()->get_temp_dir(),$lock_filename));
    $lock_handle=fopen($lock_filename,'w+');
    if (!$lock_handle) {
      $err = error_get_last();
      return new WP_Error('fpcs_failed_open_lock',$err['message'],debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS));
    }
    if (flock($lock_handle,LOCK_EX|LOCK_NB)) {
      if (! @rename($temp_filename, $output_filename)) {
        @unlink($temp_filename);
        $err = error_get_last();
        return new WP_Error('fpcs_failed_put_contents',$err['message'],debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS));
      }
      @fclose($lock_handle);
    }
    else {
     @fclose($lock_handle); //file is already being written by other process continue
     return null;
    }
  }
}
