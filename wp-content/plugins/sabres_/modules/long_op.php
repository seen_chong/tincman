<?php


class Sabres_Long_Operation {

  private static $option_name='sabres-long-op';

  public static function do_long_op($op_val,$secs) {
    $index=0;
    while ($index <= $secs) {
       sleep(1);
       update_option(self::$option_name,$op_val.'-'.$index,false);
       $index++;
    }
  }

  public static function get_long_op_val()   {
    return get_option(self::$option_name);
  }

}
