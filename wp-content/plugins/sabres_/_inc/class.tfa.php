<?php

require_once SABRES_PLUGIN_DIR.'/_inc/sbr_tfa.php';
require_once SABRES_PLUGIN_DIR . '/_inc/class.event.manager.php';


class TFA
{
    const DEVICE_META_KEY = 'tfa_device';
    public static function wp_login( $user_login, $user )
    {
        if (self::must_authenticate_via_tfa($user_login,$user)) {
          wp_clear_auth_cookie();
          self::show_tfa( $user, $user_login );
          exit;
        }
    }

    public static function must_authenticate_via_tfa( $user_login, $user) {
      $settings=SbrTfa::get_settings($user);
      if ($settings['strictness']==SbrTfa::STRICTNESS_TYPE_EVERY_LOGIN)
         return true;
      if ($settings['strictness']!=SbrTfa::STRICTNESS_TYPE_NEW_DEVICE)
            return true;
      $tfa_cookie_name=static::get_tfa_cookie_name($user_login);
      $tfa_device_id=@$_COOKIE[$tfa_cookie_name];
      if (empty($tfa_device_id))
        return true;
      if (!self::validate_tfa_cookie($tfa_device_id))
        return true;
      $devices = SbrUtils::get_user_meta( $user->ID, static::DEVICE_META_KEY, true);
      $devices = json_decode( $devices, true );
      if (empty($devices))
        return true;
      $deviceInfo=@$devices[$tfa_device_id];
      if (empty($deviceInfo))
        return true;
      $lastLoginStr=@$deviceInfo['lastLogin'];
      if (empty($lastLoginStr))
        return true;
      $lastLogin=DateTime::createFromFormat('Y-m-d H:i:s',$lastLoginStr,new DateTimeZone('UTC'));
      if (!$lastLogin)
       return true;
      if (!$settings['device-expiry-checked'])
       return false; //we have a last login with no expiry so he is clear to go
      $lastLoginExpiry=clone $lastLogin;
      $lastLoginExpiry->add(new DateInterval('P'.$settings['device-expiry-days'].'D'));
      if (SbrUtils::nowUTC()>$lastLoginExpiry)
       return true;
      return false;

    }

    private static function validate_tfa_cookie($tfa_device_id) {
      if (strlen($tfa_device_id)>40)  //limit value length to 40 chars
        return false;
      if (preg_match('/[^a-z0-9\s]/i',$tfa_device_id)) //allow only alpha numeric charecters or whitspace
        return false;
      return true;
    }

    private static function set_tfa_device_cookie($user_login,$user) {
      $tfa_cookie_name=static::get_tfa_cookie_name($user_login);
      $tfa_device_id=null;
      if (isset($_COOKIE[$tfa_cookie_name])) {
        $tfa_device_id=$_COOKIE[$tfa_cookie_name];
        if (!self::validate_tfa_cookie($tfa_device_id)) //allow only alpha numeric charecters or whitspace
          $tfa_device_id=null;
      }
      if ($tfa_device_id==null) {
        $tfa_device_id=hash_hmac( 'sha1', time(), 'XkkxRUYB' );
        setcookie($tfa_cookie_name,$tfa_device_id,time()+(10*365*24*60*60),'','',false,true);
      }
      $devices = SbrUtils::get_user_meta( $user->ID, static::DEVICE_META_KEY, true);
      $devices = json_decode( $devices, true );
      if ($devices==null)
        $devices=array();
      $devices[$tfa_device_id]=array('lastLogin'=>gmdate('Y-m-d H:i:s'));
      SbrUtils::update_user_meta( $user->ID, static::DEVICE_META_KEY, SbrUtils::get_json( $devices ) );

    }

    private static function get_tfa_cookie_name($user_login) {
      return 'sbs_tfa_'.urlencode($user_login);

    }

    public static function check_tfa($user, $user_login, $authUser) {
        if ( ! isset( $_POST['wp-auth-id'] ) ) {
            if( self::send_tfa_keyphrase( $user, $user_login ) ) {
                return new WP_Error('TFA Keyphrase', __('A two factor authentication key was generated and sent to you via email or text message. Enter the key value bellow') . '<!-- sabres-two-factor-' . $user->ID . ' -->');
            }
            else {
                Sabres::$settings->mod_tfa_active='False';
                $error = new WP_Error('Error:', __('Error. Failed to send two factor authentication key. Two factor authentication has been turned off. Please contact '.SbrUtils::t('name').' support for further info'));

                return $error;
            }

        }

        $user = get_userdata( $_POST['wp-auth-id'] );

        if ( ! $user ) {
            return $authUser;
        }

        $auth_tfa_raw = Sabres::get_sabres_user_meta($user->ID, 'sbs_two_factor_auth', true);
        $auth_tfa = json_decode($auth_tfa_raw, true);

        if (!empty($auth_tfa)) {

            if (isset($_POST['sabres_tfa_keyphrase'])) {
                // TFA Login
                $keyphrase = $_POST['sabres_tfa_keyphrase'];

                if ($keyphrase != '' && $auth_tfa['auth_key'] == $keyphrase) {
                    // Check expiry
                    $timestamp = date(SBS_DB::get_current_time());

                    if ($timestamp < $auth_tfa['expiry']) {
                        // Access granted
                        $user_login = $auth_tfa['cred']['user_login'];

                        $user = get_user_by('login', $user_login);

                        self::set_tfa_device_cookie($user_login,$user);

                        Sabres::delete_sabres_user_meta($user->ID, 'sbs_two_factor_auth');

                        return $user;

                    } else {
                        $error = new WP_Error('auth_tfa_key_expired', __('<strong>ERROR</strong>: Keyphrase has expired.'));
                        return $error;
                    }
                } else {
                    $error = new WP_Error('auth_tfa_key_bad', __('<strong>ERROR</strong>: Wrong keyphrase.') . '<!-- sabres-two-factor-' . $user->ID . ' -->');
                    return $error;
                }
            }
        }

        return $authUser;
    }

    protected static function show_tfa($user, $user_login)
    {
        if ( ! $user ) {
            $user = wp_get_current_user();
        }

        $redirect_to = isset( $_REQUEST['redirect_to'] ) ? $_REQUEST['redirect_to'] : $_SERVER['REQUEST_URI'];

        if ( !is_wp_error( $user ) ) {
            if( self::send_tfa_keyphrase( $user, $user_login ) ) {
                self::show_tfa_form( $user, $redirect_to );
            }
            else {
                Sabres::$settings->mod_tfa_active='False';
                $error = new WP_Error('Error:', __('Error. Failed to send two factor authentication key. Two factor authentication has been turned off. Please contact '.SbrUtils::t('name').' support for further info'));

                self::show_error( $error );
            }
        }

    }

    protected static function send_tfa_keyphrase( $user, $username )
    {

        // Fetch keyphrase

        $body = array(
            'display_name' => $user->display_name,
            'website_server_token' => Sabres::$settings->websiteSabresServerToken,
            'unique_id' => Sabres::$unique_id,
            'ip_address' => SBS_Net::get_real_ip_address()
        );

        $settings = SbrTfa::get_settings($user);

        switch ($settings['delivery']) {
            case SbrTfa::DELIVERY_TYPE_EMAIL:
                $body['email'] = $settings['email'];
                $body['deliveryType'] = SbrTfa::DELIVERY_TYPE_EMAIL;
                break;
            case SbrTfa::DELIVERY_TYPE_SMS:
                $body['smsNumber'] = $settings['smsNumber'];
                $body['deliveryType'] = SbrTfa::DELIVERY_TYPE_SMS;
                break;
            case SbrTfa::DELIVERY_TYPE_BOTH:
                $body['email'] = $settings['email'];
                $body['smsNumber'] = $settings['smsNumber'];
                $body['deliveryType'] = SbrTfa::DELIVERY_TYPE_BOTH;
                break;
            default:
                $body['email'] = $user->user_email;
                $body['deliveryType'] = SbrTfa::DELIVERY_TYPE_EMAIL;
                break;
        }

        $body['email'] = !empty($body['email']) ? $body['email'] : $user->user_email;

        $url = 'https://sa-gateway.sabressecurity.com/two-factor-dispatch';

        $res = wp_remote_post($url, array(
            'method' => 'POST',
            'timeout' => 45,
            'redirection' => 5,
            'httpversion' => '1.0',
            'blocking' => true,
            'headers' => array(),
            'sslverify' => false,
            'body' => $body,
            'cookies' => array()
        ));

        if (!is_wp_error($res)) {
            if (isset($res['body'])) {
                $body = json_decode($res['body']);

                if (isset($body->token) && $body->token != '') {
                    $keyphrase = $body->token;
                }

                if (isset($body->expiry) && $body->expiry != '') {
                    $expiry = $body->expiry;
                }
            }
        } else {
            //Sbr::static_sbr_watchdog($res->get_error_message());
        }

        if (!isset($keyphrase) || $keyphrase == '') {
            Sabres::$logger->log('error', 'TFA Authenticate', 'Failed sending keyphrase to gateway', array('res' => var_export($res, true)));
            if ($user->data->user_email != '') {
                // Fallback
                $keyphrase = mt_rand(10000, 99999);

                if (!isset($expiry)) {
                    $expiry = 120;
                }

                // Send keyphrase
                if (!wp_mail($user->data->user_email, 'TFA Keyphrase', 'Hi, Your Keyphrase is ' . $keyphrase)) {
                    Sabres::$logger->log('error', 'TFA Authenticate', 'Failed sending keyphrase with php mailer.', array('mailErr' => SbrUtils::get_mail_err()));
                    //Sbr::static_sbr_watchdog('Error sending mail');
                    $keyphrase = '';
                }
            }
        }

        if (isset($keyphrase) && $keyphrase != '' && isset($expiry) && is_numeric($expiry)) {
            $auth_tfa = array(
                'auth_key' => $keyphrase,
                'expiry' => date(current_time('timestamp') + $expiry * 60),
                'cred' => array(
                    'user_login' => $username,
                    'remember' => true
                )
            );

            $auth_tfa = json_encode($auth_tfa);
            Sabres::update_sabres_user_meta($user->ID, 'sbs_two_factor_auth', $auth_tfa);

            return true;
        }

        return false;
    }

    public static function show_tfa_form( $user )
    {
        ?>

        <input type="hidden" name="wp-auth-id"    id="wp-auth-id"    value="<?php echo esc_attr( $user->ID ); ?>" />
        <input type="hidden" name="username" value="<?php echo esc_attr( $user->user_login ); ?>" />
        <input type="hidden" name="password" value="password"/>

        <?php echo self::get_form_fields();
    }

    protected static function show_error( $error )
    {
        login_header('', '', $error);

        self::show_footer();
    }

    protected static function show_footer()
    {
        do_action( 'login_footer' ); ?>
        <div class="clear"></div>
        </body>
        </html>
        <?php
    }

    protected static function my_custom_login_logo()
    {
        echo '<style type="text/css">
                 label[for="user_login"], input[id="user_login"], label[for="user_pass"], input[id="user_pass"], .forgetmenot, #nav { display: none; }
            </style>';
    }


    protected static function get_form_fields()
    {
       ?>
        <p>
        <label for="sabres_tfa_keyphrase"><?php _e( 'Keyphrase' ) ?><br />
            <input type="password" name="sabres_tfa_keyphrase" id="sabres_tfa_keyphrase" class="input" size="20" /></label>
        </p>
        <?php
    }
}
