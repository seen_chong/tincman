<?php
// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) {
    exit;
}


if ( !class_exists( 'SBS_Lifecycle' ) ) {
    require_once SABRES_PLUGIN_DIR . '/_inc/modules/class.module.php';
    require_once SABRES_PLUGIN_DIR . '/_inc/server.php';
    require_once SABRES_PLUGIN_DIR.'/library/user_caps.php';

    class SBS_Lifecycle extends SBS_Module implements SBS_EventAwareInterface
    {
        protected static $instance;

        protected $server;

        protected function __construct()
        {
            $this->server = SBS_Server::getInstance();
        }

        protected function send_request($data)
        {
            $data = array_merge( $this->collect_request_data(), $data );

            $this->server->call('lifecycle-event', '', $data);
        }

        protected function collect_request_data()
        {
            $data = [
                'uniqueID' => Sabres::$unique_id,
                'readAddrCalc' => SBS_Net::get_real_ip_address(),
                'websiteServerToken' => Sabres::$settings->websiteSabresServerToken,
                'userAgent' => $_SERVER['HTTP_USER_AGENT'],
            ];

            $current_user = wp_get_current_user();

            if ( !is_wp_error( $current_user ) ) {
                $data['username'] = $current_user->user_login;
            }

            return $data;
        }

        public function register_events(SBS_EventManager $manager)
        {

            $manager->register_event_callback('login.success', function($user_login, $user) {
                if(Sabres_User_Capabilities::is_user_admin($user)) {
                    $this->send_request([
                        'event-type' => 'privileged-login',
                        'username' => $user_login,
                    ]);
                }
            });

            $manager->register_event_callback('privilege.grant', function($user_id) {
               $user = get_user_by( 'id', $user_id );

                if ( !is_wp_error( $user ) ) {
                    $this->send_request([
                        'event-type' => 'privileged-grant',
                        'targetUserName' => $user->user_login,
                    ]);
                }
            });

            $manager->register_event_callback('login.failed', function($username) {
                $this->send_request([
                    'event-type' => 'login-fail',
                    'username'  => $username,
                ]);
            });

            $manager->register_event_callback('user.register', function($user_id) {
                $user = get_user_by( 'id', $user_id );

                if ( !is_wp_error( $user ) ) {
                    $admin_caps=Sabres_User_Capabilities::intersect_admin_capabilities(array_keys($user->allcaps));

                    if ( count( $admin_caps ) ) {
                        $this->send_request([
                            'event-type' => 'privileged-grant',
                            'targetUserName' => $user->user_login,
                        ]);
                    }
                }
            });

            $manager->register_event_callback('set.user.role', function($user_id, $role, $old_roles) {
                $wp_role  = get_role( $role );
                $wp_user = get_user_by( 'id', $user_id );

                if ( !is_wp_error( $wp_role ) && !is_wp_error( $wp_user ) ) {
                    $old_caps = [];

                    foreach( $old_roles as $old_role) {
                        $wp_old_role = get_role( $old_role );
                        if ( !is_wp_error( $wp_old_role ) ) {
                            $old_caps = array_merge( $old_caps, array_keys( $wp_old_role->capabilities ) );
                        }
                    }

                    $new_caps = array_diff( array_keys( $wp_role->capabilities ), $old_caps );
                    $admin_caps= Sabres_User_Capabilities::intersect_admin_capabilities($new_caps);

                    if ( count( $admin_caps ) ) {
                        $this->send_request([
                            'event-type' => 'privileged-grant',
                            'targetUserName' => $wp_user->user_login,
                        ]);
                    }
                }
            });

            $manager->register_event_callback('add.user.role', function($user_id, $role) {
                $wp_role  = get_role( $role );
                $wp_user = get_user_by( 'id', $user_id );

                if ( !is_wp_error( $wp_role ) && !is_wp_error( $wp_user ) ) {
                    $admin_caps= Sabres_User_Capabilities::intersect_admin_capabilities(array_keys( $wp_role->capabilities ) );                    


                    if ( count( $admin_caps ) ) {
                        $this->send_request([
                            'event-type' => 'privileged-grant',
                            'targetUserName' => $wp_user->user_login,
                        ]);
                    }
                }
            });
        }

    }
}
