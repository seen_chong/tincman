<?php

if ( !defined( 'ABSPATH' ) ) {
    exit;
}
final class SBS_Firewall_Check_POST {

    public function check_request($unique_id,$settings) {
      // Server offline
      if ( strcasecmp( $settings->server_offline, 'true' ) == 0 ) return;
      // Check tokens
      if ( $settings->websiteSabresServerToken === '' ) return;
      $server=SBS_Server::getInstance();
      $data=array();
      $data['websitesabresservertoken']=$settings->websiteSabresServerToken;
      $data['uniqueid']=$unique_id;
      $this->setIfExists($data,'HTTP_CLIENT_IP');
      $this->setIfExists($data,'HTTP_X_FORWARDED_FOR');
      $this->setIfExists($data,'HTTP_X_FORWARDED');
      $this->setIfExists($data,'HTTP_FORWARDED_FOR');
      $this->setIfExists($data,'HTTP_FORWARDED');
      $this->setIfExists($data,'REMOTE_ADDR');
      $this->setIfExists($data,'SERVER_ADDR');
      $this->setIfExists($data,'REQUEST_URI');
      $this->setIfExists($data,'HTTP_USER_AGENT');
      $res=$server->call( 'validate-post-request', '', $data,array('timeout'=>30));
      if (isset($res) && is_array($res) && isset($res['response']) && isset($res['response']['code']) && isset($res['response']['code'])==200) {
        $response=json_decode($res['body'],JSON_OBJECT_AS_ARRAY);
        return $response;
      }

    }

    private function setIfExists(&$data,$key) {
      $value=@$_SERVER[$key];
      if (isset($value))
        $data[$key]=$value;
    }


}
