<?php

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) {
    exit;
}

if ( !interface_exists( 'SBS_EventAwareInterface' ) ) {
    interface SBS_EventAwareInterface
    {
        public function register_events(SBS_EventManager $manager);
    }
}