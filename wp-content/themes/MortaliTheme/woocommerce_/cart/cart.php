<?php
/**
 * Cart Page
 *
 * @author    WooThemes
 * @package   WooCommerce/Templates
 * @version     2.3.8
 */

if ( ! defined( 'ABSPATH' ) ) {
  exit; // Exit if accessed directly
}

wc_print_notices();

do_action( 'woocommerce_before_cart' ); ?>
<?php get_sidebar('checkout'); ?>
 <div id="primary" class="content-area">


<div class="cart-static-timeline">
  <ul id="checkout_timeline" class="woocommerce_checkout_timeline horizontal text">

      <li id="timeline-1" data-step="1" class="timeline cart active" >
          <div class="timeline-wrapper">
              <span class="timeline-step ">
                                      1                            </span>
              <span class="timeline-label">Cart</span>
          </div>
      </li>

      <li id="timeline-1" data-step="1" class="timeline billing" >
          <div class="timeline-wrapper">
              <span class="timeline-step ">
                                      2                            </span>
              <span class="timeline-label">Billing</span>
          </div>
      </li>

      <li id="timeline-2" data-step="2" class="timeline shipping" >
          <div class="timeline-wrapper">
              <span class="timeline-step ">
                                     3                            </span>
              <span class="timeline-label">Shipping</span>
          </div>
      </li>
      <li id="timeline-3" data-step="3" class="timeline order" >
          <div class="timeline-wrapper">
              <span class="timeline-step ">
                                     4                            </span>
              <span class="timeline-label">Order Info</span>
          </div>
      </li>
      <li id="timeline-4" data-step="4" class="timeline payment" >
          <div class="timeline-wrapper">
               <span class="timeline-step ">
                                     5                            </span>
              <span class="timeline-label">Payment</span>
          </div>
      </li>
  </ul> 

</div>

  <div class="page-wrap archive-product"> 
            <a href="/shop" class="cont_shopping">
        <button>Continue Shopping</button>
      </a>
<form action="<?php echo esc_url( WC()->cart->get_cart_url() ); ?>" method="post">

<?php do_action( 'woocommerce_before_cart_table' ); ?>

<!-- <table class="shop_table cart" cellspacing="0"> -->
  <tbody>
    <?php do_action( 'woocommerce_before_cart_contents' ); ?>

    <?php
    foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
      $_product     = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
      $product_id   = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );

      if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
        ?>
        <tr class="<?php echo esc_attr( apply_filters( 'woocommerce_cart_item_class', 'cart_item', $cart_item, $cart_item_key ) ); ?>">

          <td class="product-thumbnail  text-center">
            <?php
              $thumbnail = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key );

              if ( ! $_product->is_visible() )
                echo $thumbnail;
              else
                printf( '<a href="%s">%s</a>', $_product->get_permalink( $cart_item ), $thumbnail );
            ?>
          </td>
<div class="product-data">
          <div class="product-name">
            <?php
              if ( ! $_product->is_visible() )
                echo apply_filters( 'woocommerce_cart_item_name', $_product->get_title(), $cart_item, $cart_item_key ) . '&nbsp;';
              else
                echo apply_filters( 'woocommerce_cart_item_name', sprintf( '<a href="%s">%s </a>', $_product->get_permalink( $cart_item ), $_product->get_title() ), $cart_item, $cart_item_key );

              // Meta data
              echo WC()->cart->get_item_data( $cart_item );

                      // Backorder notification
                      if ( $_product->backorders_require_notification() && $_product->is_on_backorder( $cart_item['quantity'] ) )
                        echo '<p class="backorder_notification">' . __( 'Available on backorder', 'woocommerce' ) . '</p>';
            ?>
          </div>

          <td class="product-price text-right">
            <?php
              echo apply_filters( 'woocommerce_cart_item_price', WC()->cart->get_product_price( $_product ), $cart_item, $cart_item_key );
            ?>
          </td>

          <td class="product-quantity text-center">
            <?php
              if ( $_product->is_sold_individually() ) {
                $product_quantity = sprintf( '1 <input type="hidden" name="cart[%s][qty]" value="1" />', $cart_item_key );
              } else {
                $product_quantity = woocommerce_quantity_input( array(
                  'input_name'  => "cart[{$cart_item_key}][qty]",
                  'input_value' => $cart_item['quantity'],
                  'max_value'   => $_product->backorders_allowed() ? '' : $_product->get_stock_quantity(),
                  'min_value'   => '0'
                ), $_product, false );
              }

              echo apply_filters( 'woocommerce_cart_item_quantity', $product_quantity, $cart_item_key );
            ?>
          </td>

          <td class="product-subtotal text-right">
            <?php
              echo apply_filters( 'woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal( $_product, $cart_item['quantity'] ), $cart_item, $cart_item_key );
            ?>
          </td>

          <td class="product-remove text-center">
            <?php
              echo apply_filters( 'woocommerce_cart_item_remove_link', sprintf( '<a href="%s" class="remove" title="%s">Remove</a>', esc_url( WC()->cart->get_remove_url( $cart_item_key ) ), __( 'Remove this item', 'woocommerce' ) ), $cart_item_key );
            ?>
          </td>
        </tr>
      </div>
        <?php
      }
    }

    do_action( 'woocommerce_cart_contents' );
    ?>
    <tr>
      <td colspan="6" class="actions">
        <?php if ( WC()->cart->coupons_enabled() ) { ?>
          <div class="coupon cart-floated-col">
            <input type="text" name="coupon_code" class="input-text" id="coupon_code" value="" placeholder="<?php _e( 'Enter Promotional Code', 'woocommerce' ); ?>" /> <input type="submit" class="button" name="apply_coupon" value="<?php _e( 'Apply', 'woocommerce' ); ?>" />

            <?php do_action( 'woocommerce_cart_coupon' ); ?>
          </div>
        <?php } ?>
        <div class="update-cart cart-floated-col">
          <input type="submit" class="button" name="update_cart" value="<?php _e( 'Update Cart', 'woocommerce' ); ?>" />
        </div>

        <?php do_action( 'woocommerce_cart_actions' ); ?>

        <?php wp_nonce_field( 'woocommerce-cart' ); ?>
      </td>
    </tr>

    <?php do_action( 'woocommerce_after_cart_contents' ); ?>
  </tbody>
<!-- </table> -->

<?php do_action( 'woocommerce_after_cart_table' ); ?>

</form>

<div class="cart-collaterals">

  <?php do_action( 'woocommerce_cart_collaterals' ); ?>

</div>
</div>
</div>
<?php do_action( 'woocommerce_after_cart' ); ?>
