<?php
/**
 * Additional Information tab
 *
 * @author        WooThemes
 * @package       WooCommerce/Templates
 * @version       2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product;

$heading = apply_filters( 'woocommerce_product_additional_information_heading', __( 'Dimensions', 'woocommerce' ) );

?>

<?php if ( $heading ): ?>
	<h2><?php echo $heading; ?></h2>
	<p>Length: 11.8"</p>
	<p>Width: 3"</p>
	<p>Max Thickness: 0.3"</p>
	<p>Weight: 6 oz</p>
<?php endif; ?>

<?php $product->list_attributes(); ?>
