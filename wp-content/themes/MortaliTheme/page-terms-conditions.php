<?php get_header(); ?>
<div class="page-wrap">
	
		<div class="content-wrapper">
			<div class="page-section">
				<h1><?php the_title(); ?></h1>
				<?php the_field('content'); ?>
			</div>	
		</div>

</div>

<?php get_footer(); ?>
