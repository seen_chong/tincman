<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' :'; } ?> <?php bloginfo('name'); ?></title>

		<link href="//www.google-analytics.com" rel="dns-prefetch">
        <link href="<?php echo get_template_directory_uri(); ?>/img/icons/favicon1.ico" rel="shortcut icon">

		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="<?php bloginfo('description'); ?>">

<!--
        <link rel="stylesheet" href="/wp-content/themes/MortaliTheme/css/slick.css" type="text/css">
        <link rel="stylesheet" href="/wp-content/themes/MortaliTheme/css/slick-theme.css" type="text/css">
        
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <script type="text/javascript" src="https://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>

        <script src="/wp-content/themes/MortaliTheme/js/slick.min.js"></script>    
        <script src="https://npmcdn.com/masonry-layout@4.0/dist/masonry.pkgd.min.js"></script>
-->
        
            <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,800" rel="stylesheet">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.css" rel="stylesheet">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick-theme.css" rel="stylesheet">


    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.js"></script>
 
		<?php wp_head(); ?>
		<script>
        // conditionizr.com
        // configure environment tests
        conditionizr.config({
            assets: '<?php echo get_template_directory_uri(); ?>',
            tests: {}
        });
        </script>



	</head>
<body <?php body_class(''); ?>>
<div id="page" class="site">

    	<header>
                <div class="header-top-bar">



           <div class="header-actions"> 
                               <?php
                    wp_nav_menu( array(
                        'theme_location' => 'primary-menu'
                    ) );
                ?>
           </div>

    </div>
            <nav id="nav_left">
                <?php
                    wp_nav_menu( array(
                        'theme_location' => 'left-menu'
                    ) );
                ?>
		      </nav>
            <div class="site_branding">
                <a href="/">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/dart-frog.png" alt="Tincman">
                    <span class="header-logo-text">Tincman Herps</span>
                </a>
            </div>
            <nav id="nav_right">
                <?php
                    wp_nav_menu( array(
                        'theme_location' => 'right-menu'
                    ) );
                ?>
		      </nav>
        
        
		<div class="site-branding">
		</div><!-- .site-branding -->


	</header><!-- #masthead -->

	
    
    
    
    
    
<!--
    <div class="header-top-bar">
        <div class="header-wrapper">


           <div class="header-actions"> 
                <ul>
                    <li><a href="">Sign In / Register</a></li>
                    <li><a href="/cart">Cart <span id="cart-amount">0</span></a></li>
                </ul>
           </div>
       </div>
    </div>
-->

<!--
    <nav class="nav-bar">
        <div class="nav-wrapper">
            <div class="nav-promo">
                <p>Free domestic shipping on orders over $100.</p>
            </div>
        </div>
    </nav>
-->


