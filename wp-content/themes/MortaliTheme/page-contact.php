<?php get_header(); ?>

<div class="page_default_container">
    <div class="default_hero" style="background-image:url(<?php echo get_template_directory_uri(); ?>/img/tinc-banner.jpg)">
        <div class="overlay">
            <div class="wrapper">
                <h1><?php wp_title(''); ?></h1>
            </div>
            
        </div>
    </div>
    

    <div class="default_container">
        <div class="default_content" id="contact_page">
            <div class="wrapper">

		<?php if (have_posts()): while (have_posts()) : the_post(); ?>

			<!-- article -->
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

				<?php the_content(); ?>

				<?php comments_template( '', true ); // Remove if you don't want comments ?>

				<br class="clear">

				<?php edit_post_link(); ?>

			</article>
			<!-- /article -->

		<?php endwhile; ?>

		<?php else: ?>


		<?php endif; ?>
            </div>
        </div>
            <?php
    get_sidebar('info');?>

    </div>
</div>
    
</div>

<?php get_footer(); ?>

<?php get_footer(); ?>
