<!-- footer -->
<footer id="colophon" class="site-footer" role="contentinfo">
	<div class="footer-wrap">
		<p class="copyright">© 2016 Tincman Herps | powered by <a href="http://emagid.com/" target="_blank">eMagid</a></p>
		<div id="footer-sidebar4" class="footer-side">

			<p>
				<ul class="social-links">
					<li>
						<a href="https://www.facebook.com/tincmanherps/" target="blank"><img src="<?php echo get_template_directory_uri(); ?>/img/social-fb.png" width="25px"></a>
						<a href="https://www.instagram.com/tincman/" target="blank"><img src="<?php echo get_template_directory_uri(); ?>/img/social-ig.png" width="25px"></a>
					</li>
				</ul>
			</p>

		</div>
	</div><!-- .site-info -->
</footer><!-- #colophon -->

<div class="footer-mobile">
	<ul class="social-links">
		<li>
			<a href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/social-icon-footer.png" width="75%"></a>
		</li>
	</ul>
	<div>
		<span class="copyright">
			(212) 212-2112
		</span>
	</div>
</div>

</div>

</div><!-- /footer -->

	</div>	<!-- /wrapper -->
</div>

<!--  body-border -->
		<?php wp_footer(); ?>
		        <script type="text/javascript">
    $(document).ready(function(){
  $('.banner_container').slick({
    autoplay: true
  });
});               
</script>

<script type="text/javascript">
	$('.grid').masonry({
  // options
  itemSelector: '.grid-item',
  columnWidth: 20
});
</script>

	</body>
</html>
