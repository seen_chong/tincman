<!-- sidebar -->
<aside class="sidebar-checkout" role="complementary">


    <div class="sidebar-sections">

        <div class="section-delivery">
            <h4>Delivery and Returns:</h4>
            <p>You can choose from different delivery service labels:</p>

            <h5>Standard Shipping</h5>
            <p>4-8 working days</p>
            <p>Complimentary</p>

            <h5>Express Shipping</h5>
            <p>2-4 working days</p>
            <p>$20.00</p>
        </div>

        <div class="section-payments">
            <h4>Secure Payments:</h4>
            <p>To ensure the safety of your credit card data at all times, we use Secure Socket Layer (SSL) technology and the highest security standards, certified by Verisign&trade; </p>

            <img src="<?php echo get_template_directory_uri(); ?>/img/creditcard-icons.png" >
        </div>

        <div class="section-gifts">
            <h4>Gift Option:</h4>
            <p>Gift wrap your order, removing all prices, and add a greeting card.</p>
        </div>

        <div class="section-care">
            <h4>Customer Care:</h4>
            <p>You can reach us by:</p>

            <h5>Phone</h5>
            <p>877 804 7645</p>

            <h5>Email</h5>
            <p>Send a message</p>
        </div>

</aside>

