<?php get_header(); ?>
<header>
	<!-- banner -->
    <div class="banner_container">
        <img src="<?php echo get_template_directory_uri(); ?>/img/header_background.jpg" width="100%">

    </div>
    <div class="headline">
        <img src="<?php echo get_template_directory_uri(); ?>/img/headline_background.jpg" width="100%" height="auto">
    </div>
</header>

<div class="descrip_container">
    <div class="descrip">
    	<?php the_field('paragraph_1'); ?>
    </div>
</div>
<div class="row1_media">
	<img src="<?php echo get_template_directory_uri(); ?>/img/video_row1_video1.jpg" width="30%" class="images">
	<img src="<?php echo get_template_directory_uri(); ?>/img/video_row1_image1.jpg" width="70%" class="images">
</div>

<?php get_footer(); ?>
