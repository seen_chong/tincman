<?php get_header(); ?>

<!-- banner -->
<div class="page_front_container">
    <div class="page_front_hero" style="background-image:url(<?php the_field('banner'); ?>)">
        <div class="wrapper">
            <h1>Tincman Herps</h1>
            <p>Exotic, Unique, Top-Notch Quality</p>
            <a href="/shop" class="btn btn_transparent">Shop Now</a>
        </div>
    </div>
    
<!--
    <div class="page_front_layer_top" style="background-image:url(<?php echo get_template_directory_uri(); ?>/img/shop-banner.jpg)">
        <div class="overlay">
            <div class="wrapper">
                <div class="page_front_intro">
                    <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."</p>
                    <a href="/shop" class="btn">Shop</a>
                </div>
            </div>
        </div>
    </div>
-->

        <div class="page_front_layer_mid">
        <div class="page_front_product_switcher">

            
            <div class="product_slider">

            
<?php
$params = array('posts_per_page' => 5, 'post_type' => 'product');
$wc_query = new WP_Query($params);
?>

     <?php if ($wc_query->have_posts()) : ?>
     <?php while ($wc_query->have_posts()) :
                $wc_query->the_post(); ?>
     <div class="switcher_center">
         <?php the_post_thumbnail(); ?>
         
         <div class="slide_action">
          <h5>
               <a href="<?php the_permalink(); ?>">
               <?php the_title(); ?>
               </a>
            </h5>
                <p>
                    <?php 
                    global $post;
                    $product = new WC_Product($post->ID); 
                    echo     wc_price($product->get_price_including_tax(1,$product->get_price()));
                    ?>
                </p>
         <a href="<?php the_permalink(); ?>" class="btn btn_inverse">View Now</a>
         </div>

     </div>
     <?php endwhile; ?>
     <?php wp_reset_postdata(); ?>
     <?php else:  ?>

          <?php _e( 'No Products' ); ?>

     <?php endif; ?>
            </div>
                

            </div>
<script>
    $(document).ready(function(){
      $('.product_slider').slick({
            centerMode: true,
            slidesToShow: 3,
          arrows: false,
          dots: true,
            responsive: [
    {
      breakpoint: 768,
      settings: {
        arrows: false,
        centerMode: true,
        centerPadding: '40px',
        slidesToShow: 3
      }
    },
    {
      breakpoint: 480,
      settings: {
        arrows: false,
        centerMode: true,
        centerPadding: '40px',
        slidesToShow: 1
      }
    }
  ]

      });
    });
			    
</script>
        

        </div>


    <div class="instagram_board">
        <h1>Check Out Our Social Board</h1>
        <div class="social_img" style="background-image:url(<?php the_field('media_1'); ?>)"></div>
        <div class="social_img" style="background-image:url(<?php the_field('media_2'); ?>)"></div>
        <div class="social_img" style="background-image:url(<?php the_field('media_3'); ?>)"></div>

    </div>



<div class="page-wrap">


    <div class="active-blocks"> 
        <div class="active-blocks-wrapper">
        
            <div class="active-block-1">
                <h3>Most Popular</h3>
                <div class="banner_container">
                    <div >
                        <a href="/product/tincman-herps-16-mixed-spectrum-20-watt-led/">
                            <img src="/wp-content/uploads/2016/08/IMG_0039-e1538761978607-600x600.jpg" width="200px">
                        </a>
                        <h5>16” Led Mixed Spectrum</h5>
                        <p>$47.00</p>
                    </div>
                    <div>
                        <a href="/product/tincman-herps-handmade-smoothie-media-50-serving/">
                            <img src="/wp-content/uploads/2016/08/IMG_9963-e1538759799640-600x600.jpg" width="200px">
                        </a>
                        <h5>50 Serving Media</h5>
                        <p>$42.00</p>
                    </div>
                </div>
            </div>

<!--
            <div class="active-block-2">
                <div class="ig-shots">
                    <a href="">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/tinc-banner-3.jpg">
                    </a>
                    <button>View Now</button>
                </div>
            </div>
-->

<!--
            <div class="active-block-3" style="background-image:url(<?php echo get_template_directory_uri(); ?>/img/clearance-bg.png")>
                <h4>Take an additional</h4>
                <div></div>
                <h2><span class="grand-text">30</span></h2>
                <h3><span class="grand-text">%</span> <br>off</h3>
                <p><strong>all dart frogs</strong> with code <strong>tinc at checkout</strong></p>
                <a href="/shop/">
                    <button>Shop Dart Frogs</button>
                </a>
            </div>
-->

        </div>
    </div>
    
</div>



<?php get_footer(); ?>
