<!-- sidebar -->
<aside class="sidebar" role="complementary">

 <div class="filter_list">
 	<ul>
 		<li id="master-filter"><a href="">Filters</a></li>
        <?php
            if(is_active_sidebar('filter-widget')){
                dynamic_sidebar('filter-widget');
            }
        ?>
    </ul>

 </div>


</aside>
<!-- /sidebar -->
<?php get_footer(); ?>



